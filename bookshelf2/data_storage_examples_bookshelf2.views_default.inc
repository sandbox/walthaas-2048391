<?php

/**
 * @file
 * Views built in to the data_storage_examples_book2 module
 */

/**
 * Define the views
 */
function data_storage_examples_bookshelf2_views_default_views() {
  
$views = array();  

$view = new view();
$view->name = 'bookshelf2';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Bookshelf2';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Bookshelf2';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'title' => 'title',
  'book_authors' => 'book_authors',
  'pub_date' => 'pub_date',
  'publisher' => 'publisher',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'book_authors' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'pub_date' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'publisher' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Author(s) */
$handler->display->display_options['fields']['book_authors']['id'] = 'book_authors';
$handler->display->display_options['fields']['book_authors']['table'] = 'field_data_book_authors';
$handler->display->display_options['fields']['book_authors']['field'] = 'book_authors';
$handler->display->display_options['fields']['book_authors']['delta_offset'] = '0';
/* Field: Content: Publication date */
$handler->display->display_options['fields']['pub_date']['id'] = 'pub_date';
$handler->display->display_options['fields']['pub_date']['table'] = 'field_data_pub_date';
$handler->display->display_options['fields']['pub_date']['field'] = 'pub_date';
$handler->display->display_options['fields']['pub_date']['settings'] = array(
  'format_type' => 'date_only',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
);
/* Field: Content: Publisher */
$handler->display->display_options['fields']['publisher']['id'] = 'publisher';
$handler->display->display_options['fields']['publisher']['table'] = 'field_data_publisher';
$handler->display->display_options['fields']['publisher']['field'] = 'publisher';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'book2' => 'book2',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Body (body) */
$handler->display->display_options['filters']['body_value']['id'] = 'body_value';
$handler->display->display_options['filters']['body_value']['table'] = 'field_data_body';
$handler->display->display_options['filters']['body_value']['field'] = 'body_value';
$handler->display->display_options['filters']['body_value']['operator'] = 'contains';
$handler->display->display_options['filters']['body_value']['group'] = 1;
$handler->display->display_options['filters']['body_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['body_value']['expose']['operator_id'] = 'body_value_op';
$handler->display->display_options['filters']['body_value']['expose']['label'] = 'Body';
$handler->display->display_options['filters']['body_value']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['body_value']['expose']['operator'] = 'body_value_op';
$handler->display->display_options['filters']['body_value']['expose']['identifier'] = 'body_value';
$handler->display->display_options['filters']['body_value']['expose']['remember'] = TRUE;
$handler->display->display_options['filters']['body_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
);
/* Filter criterion: Content: Author(s) (book_authors) */
$handler->display->display_options['filters']['book_authors_value']['id'] = 'book_authors_value';
$handler->display->display_options['filters']['book_authors_value']['table'] = 'field_data_book_authors';
$handler->display->display_options['filters']['book_authors_value']['field'] = 'book_authors_value';
$handler->display->display_options['filters']['book_authors_value']['operator'] = 'contains';
$handler->display->display_options['filters']['book_authors_value']['group'] = 1;
$handler->display->display_options['filters']['book_authors_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['book_authors_value']['expose']['operator_id'] = 'book_authors_value_op';
$handler->display->display_options['filters']['book_authors_value']['expose']['label'] = 'Author(s)';
$handler->display->display_options['filters']['book_authors_value']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['book_authors_value']['expose']['operator'] = 'book_authors_value_op';
$handler->display->display_options['filters']['book_authors_value']['expose']['identifier'] = 'book_authors_value';
$handler->display->display_options['filters']['book_authors_value']['expose']['remember'] = TRUE;
$handler->display->display_options['filters']['book_authors_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
);
/* Filter criterion: Content: Publication date (pub_date) */
$handler->display->display_options['filters']['pub_date_value']['id'] = 'pub_date_value';
$handler->display->display_options['filters']['pub_date_value']['table'] = 'field_data_pub_date';
$handler->display->display_options['filters']['pub_date_value']['field'] = 'pub_date_value';
$handler->display->display_options['filters']['pub_date_value']['operator'] = '>=';
$handler->display->display_options['filters']['pub_date_value']['group'] = 1;
$handler->display->display_options['filters']['pub_date_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['pub_date_value']['expose']['operator_id'] = 'pub_date_value_op';
$handler->display->display_options['filters']['pub_date_value']['expose']['label'] = 'Publication date';
$handler->display->display_options['filters']['pub_date_value']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['pub_date_value']['expose']['operator'] = 'pub_date_value_op';
$handler->display->display_options['filters']['pub_date_value']['expose']['identifier'] = 'pub_date_value';
$handler->display->display_options['filters']['pub_date_value']['expose']['remember'] = TRUE;
$handler->display->display_options['filters']['pub_date_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
);
$handler->display->display_options['filters']['pub_date_value']['year_range'] = '-10:+3';
/* Filter criterion: Content: Publisher (publisher) */
$handler->display->display_options['filters']['publisher_value']['id'] = 'publisher_value';
$handler->display->display_options['filters']['publisher_value']['table'] = 'field_data_publisher';
$handler->display->display_options['filters']['publisher_value']['field'] = 'publisher_value';
$handler->display->display_options['filters']['publisher_value']['operator'] = 'contains';
$handler->display->display_options['filters']['publisher_value']['group'] = 1;
$handler->display->display_options['filters']['publisher_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['publisher_value']['expose']['operator_id'] = 'publisher_value_op';
$handler->display->display_options['filters']['publisher_value']['expose']['label'] = 'Publisher';
$handler->display->display_options['filters']['publisher_value']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['publisher_value']['expose']['operator'] = 'publisher_value_op';
$handler->display->display_options['filters']['publisher_value']['expose']['identifier'] = 'publisher_value';
$handler->display->display_options['filters']['publisher_value']['expose']['remember'] = TRUE;
$handler->display->display_options['filters']['publisher_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'bookshelf2';

$views[] = $view;
return $views;
}