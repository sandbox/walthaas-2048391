<?php

/**
 * @file
 * Install/uninstall the bookshelf1 example module
 */

/**
 * Implement hook_install().
 * 
 * Change the plain_text format to allow limited HTML.
 * 
 * Create the book1 content type and populate the Drupal database with
 * book1 nodes containing information about a dozen assorted Drupal books.
 */
function data_storage_examples_bookshelf1_install() {
  
  // Equivalent to:
  // Administration >> Configuration >> Text formats >> Plain text
  // Check box "Limit allowed HTML tags"
  // Uncheck box "Display any HTML as plain text"
  db_update('filter')
    ->fields(array('status' => 1))
    ->condition('format', 'plain_text')
    ->condition('name', 'filter_html')
    ->execute();
  db_update('filter')
    ->fields(array('status' => 0))
    ->condition('format', 'plain_text')
    ->condition('name', 'filter_html_escape')
    ->execute();
  
  $books = array(
    array(
      'title'   => 'Drupal 7 Multi Sites Configuration',
      'value'   => "<ul>\n"
        . "  <li>\n"
        . "    Prepare your server for hosting multiple sites\n"
        . "  </li>\n"
        . "  <li>\n"
        . "    Configure and install several sites on one instance of Drupal\n"
        . "  </li>\n"
        . "  <li>\n"
        . "    Manage and share themes and modules across the multi-site"
        . "    configuration\n"
        . "  </li>\n"
        . "</ul>\n"
        . "Drupal is one of the most powerful PHP Content Management Systems"
        . " there is. However, why would you install a CMS for every site you"
        . " build? With just one copy of Drupal you can host several sites."
        . " Drupal has long had multi-site support, and Drupal 7’s support is"
        . " even better than previous versions.\n"
        . "\n"
        . "This book will show you how to configure a system for multi-site,"
        . " and then install several sites on one copy of Drupal, all with"
        . " their own domain name and database. Learn how to install and share"
        . " modules and themes, configure Apache, PHP, and MySQL for"
        . " multi-site, and then manage the site. Once your site system is"
        . " successfully set up, discover some of the advanced configurations"
        . " possible with Drupal multi-site, as well as how to upgrade and"
        . " maintain your sites.\n"
        . "\n"
        . "Author: Matt Butcher\n"
        . "Publisher: Packt Publishing\n"
        . "Date: March 26, 2012\n"
        . "ISBN: 978-1849518000\n"
        . "URL: http://www.packtpub.com/drupal-7-multi-sites-configuration/book\n"
    ),
    array(
      'title'   => "Drupal 7 Module Development",
      'value'   => "<ul><li>Specifically written for Drupal 7 development</li>"
         . "<li>Write your own Drupal modules, themes, and libraries</li>"
         . "<li>Discover the powerful new tools introduced in Drupal 7</li>"
         . "<li>Learn the programming secrets of six experienced Drupal developers</li>"
         . "<li>Get practical with this book's project-based format</li></ul>"
        . "Drupal is an award-winning open-source Content Management System."
        . " It's a modular system, with an elegant hook-based architecture, and"
        . " great code. Modules are plugins for Drupal that extend, build or"
        . " enhance Drupal core functionality.\n"
        . "\n"
        . "In Drupal 7 Module development book, six professional Drupal"
        . " developers use a practical, example-based approach to introduce"
        . " PHP developers to the powerful new Drupal 7 tools, APIs, and"
        . " strategies for writing custom Drupal code.\n"
        . "\n"
        . "These tools not only make management and maintenance of websites"
        . " much easier, but they are also great fun to play around with and"
        . " amazingly easy to use.\n"
        . "\n"
        . "If you're eager to learn about these new APIs and start creating"
        . " modules for Drupal 7, this is your book. Walk through the"
        . " development of complete Drupal modules with this primer for PHP"
        . " programmers.\n"
        . "\n"
        . "From basic modules and themes to sophisticated Drupal extensions,"
        . " learn how to use Drupal's API and hook system to build powerful"
        . " custom Drupal websites. With an emphasis on practical programming,"
        . " this book takes a project-based approach, providing working"
        . " examples in every chapter.\n"
        . "\n"
        . "Specifically written for Drupal 7, this book will get you coding"
        . " modules as quickly as possible, and help you add the features that"
        . " will give your work that professional gloss!\n"
        . "\n"
        . "This book will walk you through the development of complete Drupal"
        . " modules and show you how to add various features to meet your"
        . " requirements.\n"
        . "\n"
        . "The Drupal content management system, written in the popular PHP"
        . " language, has skyrocketed in popularity.\n"
        . "\n"
        . "Developers for this system are in high demand. This book prepares"
        . " PHP developers for Drupal development, explaining architecture,"
        . " exploring APIs, and emphasizing practical approaches.\n"
        . "\n"
        . "In each chapter, readers will learn new aspects of the system while"
        . " creating fully-functioning modules, themes, and libraries. Learn"
        . " how to “hook into” any part of the Drupal process, creating custom"
        . "content types, extending existing capabilities, and integrating with"
        . " external services and applications.\n"
        . "\n"
        . "Learn the ins and outs of writing custom modules, themes,"
        . " installation profiles and libraries for the Drupal PHP content"
        . " management system."
        . "\n"
        . "Authors: Matt Butcher, Larry Garfield, John Albin Wilkins,"
        . " Matt Farina, Ken Rickard, Greg Dunlap\n"
        . "Publisher: Packt Publishing\n"
        . "Date: December 3, 2010\n"
        . "ISBN: 978-1849511162\n"
        . "URL: http://www.packtpub.com/drupal-7-module-development/book\n",
    ),
    array(
      'title'   => "Learning Drupal 6 Module Development: A practical tutorial"
        . " for creating your first Drupal 6 modules with PHP",
      'value'   => "<ul><li>Specifically written for Drupal 6 development</li>"
        . "<li>Program your own Drupal modules</li>"
        . "<li>No experience of Drupal development required</li>"
        . "<li>Know Drupal 5? Learn what's new in Drupal 6</li>"
        . "<li>Integrate AJAX functionality with the jQuery library</li>"
        . "<li>Packt donates a percentage of every book sold to the Drupal foundation</li></ul>"
        ."With great power comes... tremendous flexibility. Drupal is an"
        . " award-winning open-source Content Management System, and the"
        . " feature-packed sixth release is right around the corner. It's a"
        . " modular system, with an elegant hook-based architecture, and great"
        . " code. These are a few of the perks that make Drupal a choice"
        . " platform for developers who want the power of an established CMS,"
        . " but the freedom to make it work for them. From social networking"
        . " to AJAX to e-commerce, the hundreds of existing modules attest to"
        . " Drupal's flexibility. When you create a new module for Drupal, it"
        . " fits seamlessly into the look and feel of your overall site. If you"
        . " can think it in PHP, you can code it as a Drupal module.\n"
        . "\n"
        . "Dive into Drupal module development as we create the Philosopher"
        . " Biographies website, developing new modules and themes to meet"
        . " practical goals. Create custom content types. Add AJAX"
        . " functionality with the jQuery library. Use external XML APIs to add"
        . " content to your site. Email newsletters to site members. Use"
        . " themes. Empower administrators with extra features. And bundle it"
        . " all up in a custom installation profile. You won't find a \"hello"
        . " world\" here!\n"
        . "\n"
        . "If you're eager to start creating modules for Drupal 6, this is your"
        . " book. Walk through the development of complete Drupal modules with"
        . " this primer for PHP programmers. Specifically written for Drupal 6,"
        . " this book will get you coding modules as quickly as possible, and"
        . " help you add the features that will give your work that"
        . " professional gloss!\n"
        . "\n"
        . "Just getting started with Drupal development? This book will give"
        . " you a clear, concise and, of course, practical guidance to take you"
        . " from the basics of creating your first module to developing the"
        . " skills to make you a Drupal developer to be reckoned with.\n"
        . "\n"
        . "Are you a Drupal developer looking to update to version 6? This book"
        . " covers the new and updated APIs to guide your transition to Drupal"
        . " 6. The new menu system, the Forms and Schema APIs, and many core"
        . " revisions are covered in this book.\n"
        . "\n"
        . "Walk through the development of complete Drupal 6 modules with this"
        . " primer for PHP programmers, written specifically for Drupal 6 to"
        . " get you started coding your first module.\n"
        . "\n"
        . "Are you a Drupal developer looking to update to version 6? This book"
        . " covers the new and updated APIs to guide your transition to Drupal"
        . " 6. The new menu system, the Forms and Schema APIs, and many core"
        . " revisions are covered in this book.\n"
        . "\n"
        . "Author: Matt Butcher\n"
        . "Publisher: Packt Publishing\n"
        . "Date: May 8, 2008\n"
        . "ISBN: 978-1847194442\n"
        . "URL: http://www.packtpub.com/learning-drupal-6-module-development/book\n",
    ),
    array(
      'title'   => 'Pro Drupal Development',
      'value'   => "Drupal is one of the most popular content management systems in use"
        . " today. With it, you can create a variety of community-driven sites,"
        . " including blogs, forums, wiki-style sites, and much more. Pro"
        . " Drupal Development was written to arm you with knowledge to"
        . " customize your Drupal installation however you see fit. The book"
        . " assumes that you already possess the knowledge to install and bring"
        . " a standard installation online. Then authors John VanDyk and Matt"
        . " Westgate delve into Drupal internals, showing you how to truly take"
        . " advantage of its powerful architecture.\n"
        . "\n"
        . "You'll learn how to create your own modules, develop your own"
        . " themes, and produce your own filters. You'll learn the inner"
        . " workings of each key part of Drupal, including user management,"
        . " sessions, the node system, caching, and the various APIs available"
        . " to you. Of course, your Drupal-powered site isnt effective until"
        . " you can efficiently serve pages to your visitors. As such, the"
        . " authors have included the information you need to optimize your"
        . " Drupal installation to perform well under high-load situations."
        . " Also featured is information on Drupal security and best practices,"
        . " as well as integration of Ajax and the internationalization of your"
        . " Drupal web site. Simply put, if you are working with Drupal at all,"
        . " then you need this book.\n"
        . "<ul><li>This book is written by Drupal core developers.</li>"
        . "<li>Drupal architecture and behavior are mapped out visually.</li>"
        . "<li>Common pitfalls are identified and addressed.</li>"
        . "<li>Chapters provide regular discussion and reference to why things work"
        . " they way they do, not just how.</li>"
        ."<li>Features a foreword by Dries Buytaert, Drupal founder.</li></ul>"
        . "\n"
        . "Author: John K. VanDyk, Matt Westgate\n"
        . "Publisher: Apress\n"
        . "Date: April 16, 2007\n"
        . "ISBN: 978-1-59059-755-2\n"
        . "URL: http://www.apress.com/9781590597552\n",
    ),
    array(
      'title'   => 'Pro Drupal Development, Second Edition',
      'value'   => "<ul><li>Find out how to create your own modules, develop your own"
        . " themes, and produce your own filters.</li>"
        . "<li>Learn the inner workings of each key part of Drupal, including user"
        . " management, sessions, the node system, caching, and the various"
        . " APIs available to you.</li>"
        . "<li>Discover how to optimize your Drupal installation to perform well"
        . " under high–load situations.</li>"
        . "<li>Gain the knowledge needed to secure your Drupal installation and"
        . " other best practice techniques.</li>"
        . "<li>Learn to integrate Ajax into and internationalize your Drupal web"
        . " site.</li></ul>"
        . "Widely praised for its in–depth coverage of Drupal internals,"
        . " bestselling Pro Drupal Development has been updated for Drupal 6 in"
        . " this edition, and provides are even more tricks of the trade to"
        . " help you further yourself as a professional Drupal developer.\n"
        . "\n"
        . "Assuming you already know how to install and bring a standard"
        . " installation online, John K. VanDyk gives you everything else you"
        . " need to customize your Drupal installation however you see fit. Pro"
        . " Drupal Development, Second Edition delves deep into Drupal"
        . " internals, showing you how to take full advantage of its powerful"
        . " architecture.\n"
        . "\n"
        . "Author: John K. VanDyk\n"
        . "Publisher: Apress\n"
        . "Date: August 21, 2008\n"
        . "ISBN: 978-1-4302-0989-8\n"
        . "URL: http://www.apress.com/9781430209898\n",
    ),
    array(
      'title'   => 'Pro Drupal 7 Development, Third Edition',
      'value'   => "Pro Drupal 7 Development is geared toward professionals who"
        . " need to deploy and customize Drupal. This book delves deep into the"
        . " internals of the Drupal 7 system and teaches you everything you"
        . " need to know to deploy a highly customized and optimized Drupal"
        . " installation.\n"
        . "\n"
        . "Pro Drupal 7 Development updates the most popular development"
        . " reference for the release of Drupal 7. With several new and"
        . " completely-rewritten essential APIs and improvements in Drupal 7,"
        . " this book will not only teach developers how to write modules"
        . " ranging from simple to complex, but also how Drupal itself works.\n"
        . "\n"
        . "This book is for existing Drupal module developers wanting to learn"
        . " about Drupal 7 and people already knowledgeable in PHP who are"
        . " interested in developing for Drupal. \n"
        . "\n"
        . "Authors: Todd Tomlinson, John VanDyk\n"
        . "Publisher: Apress\n"
        . "Date: December 29, 2010\n"
        . "ISBN: 978-1-4302-2838-7\n"
        . "URL: http://www.apress.com/9781430228387\n",
    ),
    array(
      'title'   => "Drush User's Guide",
      'value'   => "<ul><li>Stop clicking around administration pages and start issuing"
        . " commands straight to your Drupal sites.</li>"
        . "<li>Write your own commands, hook in to alter existing ones and extend"
        . " the toolkit with a long list of contributed modules.</li>"
        . "<li>A practical guide full of examples and step-by-step instructions to"
        . " start using Drush right from Chapter 1.</li></ul>"
        . "Drush is a command line interface for Drupal. Most of the tasks for"
        . " building and maintaining a website are repetitive and involve"
        . " filling in forms on administration pages. The majority of these"
        . " tasks can be achieved with a single Drush command, shortening the"
        . " development and maintenance time of a project drastically.\n"
        . "\n"
        . "Drush User’s Guide will allow you to be more productive and"
        . " efficient in building and maintaining your Drupal sites through the"
        . " command line. You will learn to install Drush on different"
        . " platforms, and manage and configure your Drupal site by learning"
        . " how to use and create Drush commands.\n"
        . "\n"
        . "Become a Drush expert by exploring its command toolkit; customizing"
        . " it to suit your needs, and extending it with contributed modules.\n"
        . "\n"
        . "The command line will allow you to download, enable and upgrade"
        . " Drupal projects in seconds. Back up your files, code and data in"
        . " one single file, clear the cache, interact with databases, and"
        . " deploy sites to remote machines – all using simply the command"
        . " line. Use Drush with your own commands or alter existing ones; and"
        . " extend the toolkit with a long list of contributed modules.\n"
        . "\n"
        . "Drush User’s guide has everything you need to extend your use of the"
        . " command line to easily build and manage your Drupal sites.\n"
        . "\n"
        . "Author: Requena Juan Pablo Novillo\n"
        . "Publisher: Packt Publishing\n"
        . "Date: April 10, 2012\n"
        . "ISBN: 978-1849517980\n"
        . "URL: http://www.packtpub.com/drush-drupal-command-line-interface-users-guide/book\n",
    ),
    array(
      'title'   => "Drupal 7 Development by Example Beginner's Guide",
      'value'   => "<ul><li>A hands-on, example-driven guide to programming Drupal"
        . " websites</li>"
        . "<li>Discover a number of new features for Drupal 7 through practical and"
        . " interesting examples while building a fully functional recipe"
        . " sharing website</li>"
        . "<li>Learn about web content management, multi-media integration, and"
        . " e-commerce in Drupal 7</li></ul>"
        . "Drupal is a powerful PHP content management system that allows you"
        . " to build a wide variety of websites. By combining the power of"
        . " HTML5, JavaScript, and CSS, you can develop and customize a"
        . " fully-functional, world-class website. This book also covers some"
        . " important changes from Drupal 6 to version 7, so even experienced"
        . " Drupal users will find this straightforward guide useful.\n"
        . "\n"
        . "Drupal 7 Development by Example Beginner’s Guide has numerous code"
        . " examples that will not only introduce new Drupal 7 development"
        . " concepts, but will give you the skills needed to build a"
        . " world-class Drupal website. By following the development of a"
        . " recipe-sharing, e-commerce site, you will quickly and easily get"
        . " your own Drupal site up and running.\n"
        . "\n"
        . "Starting from a solid Drupal 7development environment, this book"
        . " will show you how to extend Drupal with front- end code using Ajax,"
        . " jQuery, and server side PHP. In addition to learning how to"
        . " integrate HTML5, the book will cover responsive web design, and"
        . " cutting edge CSS3. Using the example of an e-commerce and social"
        . " networking site, this book will develop your Drupal programming"
        . " skills so that you will be capable of developing advanced code good"
        . " enough to be shared with the Drupal community.\n"
        . "\n"
        . "Author: Kurt Madel\n"
        . "Publisher: Packt Publishing\n"
        . "Date: May 23, 2012\n"
        . "ISBN: 978-1849516808\n"
        . "URL: http://www.packtpub.com/drupal-7-development-by-example-beginners-guide/book\n",
    ),
    array(
      'title'   => 'Drupal 7: the Essentials',
      'value'   => "You know Drupal is the best web publishing platform on"
        . " Planet. You have started using Drupal 7, built a site or two, and"
        . " maybe even written a module. But if you don't know Drupal's"
        . " essential modules inside out, you don't know Drupal.\n"
        . "\n"
        . "With this book you will learn:"
        . "<ul>"
        . "  <li>"
        . "    How to rewrite fields and combine contextual filters with Views"
        . "  </li>"
        . "  <li>"
        . "    How to set up Flag and access its data using views relationships"
        . "  </li>"
        . "  <li>"
        . "    How to automate and schedule customized reactions on your site using"
        . "    Rules"
        . "  </li>"
        . "  <li>"
        . "    How to use Page manager to master contextual information on your"
        . "     site"
        . "  </li>"
        . "  <li>"
        . "    How to use Panels and Views content panes to display the right data"
        . "    in the right places."
        . "  </li>"
        . "</ul>"
        . "Authors: Johan Falk, NodeOne, Eva Hilden Smith, Matts"
        . " Hilden\n"
        . "Publisher: CreateSpace Independent Publishing Platform\n"
        . "Date: June 30, 2011\n"
        . "ISBN: 978-1463659714\n"
        . "URL: http://www.amazon.com/Drupal-7-Essentials-Johan-Falk/dp/1463659717\n",
    ),
    array(
      'title'   => "Drupal User's Guide: Building and Administering a"
        . " Successful Drupal-Powered Web Site",
      'value'   => "Finally, Drupal Made Easy: A Step-By-Step Guide from"
        . " Planning to Finished Site\n"
        . "\n"
        . "The open source content management system Drupal offers amazing"
        . " flexibility, sophistication, and power. The catch? Many first-time"
        . " users find it difficult to get started, and most Drupal books don’t"
        . " help with the initial stages. Drupal™ User’s Guide is different:"
        . " easy to use, fun to read, practical, and complete!\n"
        . "\n"
        . "Long-time Drupal site developer Emma Jane Hogbin guides you through"
        . " every step of building sites with Drupal, from installation and"
        . " site planning through launching your first site. Drawing on her"
        . " experience teaching thousands of beginners, she covers both Drupal"
        . " and Web design tasks, showing exactly how they fit together.\n"
        . "\n"
        . "Drupal™ User’s Guide shows how to use Drupal 7’s newest improvements"
        . " to build more modern, manageable sites for any business or"
        . " organization. Hogbin covers crucial topics other Drupal books"
        . " ignore, including search engine optimization and accessibility.\n"
        . "\n"
        . "Author: Emma Jane Hogbin\n"
        . "Publisher: Pearson Education\n"
        . "Date: September 29, 2011\n"
        . "ISBN: 978-0137041299\n"
        . "URL: http://www.pearsoned.co.uk/bookshop/detail.asp?item=100000000362501\n",
    ),
    array(
      'title'   => 'Design and Prototyping for Drupal',
      'value'   => "Itching to build interesting projects with Drupal, but"
        . " confused by the way it handles design challenges? This concise"
        . " guide helps small teams and solo website designers understand how"
        . " Drupal works by demonstrating the ways it outputs content. You’ll"
        . " learn how to manage Drupal’s output, design around it, and then"
        . " turn your design into a theme.\n"
        . "\n"
        . "In the second of three volumes on Drupal design, award-winning"
        . " designer Dani Nordin takes you beyond basic site planning and"
        . " teaches you key strategies for working with themes, layouts, and"
        . " wireframes. Discover how to use Drupal to make your vision a"
        . " reality, instead of getting distracted by the system’s project and"
        . " code management details.\n"
        . "<ul>"
        . "  <li>"
        . "    Learn strategies for sketching, wireframing, and designing"
        . "    effective layouts"
        . "  </li>"
        . "  <li>"
        . "    Break down a Drupal layout to understand its basic components"
        . "  </li>"
        . "  <li>"
        . "    Understand Drupal’s theme layer, and what to look for in a base"
        . "    theme"
        . "  </li>"
        . "  <li>"
        . "    Work with the 960 grid system to facilitate efficient wireframing"
        . "    and theming"
        . "  </li>"
        . "  <li>"
        . "    Manage Drupal markup, including the code generated by the powerful"
        . "    Views module"
        . "  </li>"
        . "  <li>"
        . "    Use LessCSS to organize CSS and help you theme your site more"
        . "    efficiently"
        . "  </li>"
        . "</ul>"
        . "Author: Dani Nordin\n"
        . "Publisher: O'Reilly Media\n"
        . "Date: December 24, 2011\n"
        . "ISBN: 978-1449305505\n"
        . "URL: http://shop.oreilly.com/product/0636920020295.do\n",
    ),
    array(
      'title'   => 'Mapping With Drupal',
      'value'   => "Build beautiful interactive maps on your Drupal website,"
        . " and tell engaging visual stories with your data. This concise guide"
        . " shows you how to create custom geographical maps from top to"
        . " bottom, using Drupal 7 tools and out-of-the-box modules. You’ll"
        . " learn how mapping works in Drupal, with examples on how to use"
        . " intuitive interfaces to map local events, businesses, groups, and"
        . " other custom data.\n" 
        . "\n"
        . "Although building maps with Drupal can be tricky, this book helps"
        . " you navigate the system’s complexities for creating sophisticated"
        . " maps that match your site design. Get the knowledge and tools you"
        . " need to build useful maps with Drupal today.\n"
        . "<ul>"
        . "  <li>"
        . "    Get up to speed on map projections, the ethics of making maps,"
        . "    and the challenges of building them online"
        . "  </li>"
        . "  <li>"
        . "    Learn how spatial data is stored, input by users, manipulated,"
        . "    and queried"
        . "  </li>"
        . "  <li>"
        . "    Use the OpenLayers or GMap modules to display maps with lists,"
        . "    tables, and data feeds\n"
        . "  </li>"
        . "  <li>"
        . "    Create rich, custom interactions by applying geolocation\n"
        . "  </li>"
        . "  <li>"
        . "    Customize your map’s look and feel with personalized markers, map"
        . "    tiles, and map popups\n"
        . "  </li>"
        . "  <li>"
        . "    Build modules that add imaginative and engaging interactions\n"
        . "  </li>"
        . "</ul>"
        . "\n"
        . "Authors: Alan Palazzolo, Thomas Turnbull\n"
        . "Publisher: O'Reilly Media\n"
        . "Date: December 28, 2011\n"
        . "ISBN: 978-1449308940\n"
        . "URL: http://shop.oreilly.com/product/0636920021230.do\n",
    ),
  );
  
  // Create the book1 node type
  $book1 = array(
    'type'        => 'book1',
    'name'        => t('Book1'),
    'base'        => 'node_content',
    'description' => t('Content type to hold a book description'),
    'has_title'   => TRUE,
    'custom'      => FALSE,
  );
  $book1_type = node_type_set_defaults($book1);
  node_add_body_field($book1_type);
  node_type_save($book1_type);
  
  // Add some content of the book1 type
  foreach($books as $book) {
    $new_book = new stdClass();
    $new_book->type = 'book1';
    // Don't allow comments
    $new_book->comment = 0;
    // Don't promote to front page
    $new_book->promote = 0;
    // Publish the node
    $new_book->status = 1;
    // Language is undefined
    $new_book->language = LANGUAGE_NONE;
    // Not sticky at top of page
    $new_book->sticky = 0;
    // Attribute the content to user 1
    $new_book->uid = 1;
    $new_book->title = $book['title'];
    $new_book->body[LANGUAGE_NONE][0]['value'] = $book['value'];
    $new_book->body[LANGUAGE_NONE][0]['format'] = 'plain_text';
    node_save($new_book);
  }
}


/**
 * Implement hook_uninstall().
 * 
 * We don't have a way to reverse the changes to the Plain text format because
 * we don't know what the condition was before the module was installed.
 */
function data_storage_examples_bookshelf1_uninstall() {
  
  // Delete the nodes with content type == 'book1'
  $query = new EntityFieldQuery();
  $entities = $query->entityCondition('entity_type', 'node')
    ->propertyCondition('type', 'book1')
    ->execute();
  if (!empty($entities['node'])) {
    node_delete_multiple(array_keys($entities['node']));
  }
  
  // Delete the book1 node type
  node_type_delete('book1');
  
  // Delete variables for this content type
  variable_del('additional_settings__active_tab_book1');
  variable_del('node_options_book1');
  variable_del('node_preview_book1');
  variable_del('node_submit_book1');
  variable_del('node_submitted_book1');

}